# Sourcecode von benjamindornfeld.de

Dieses Repository enthält den Sourcecode für die Website benjamindornfeld.de. Der Code ist großteilig von https://templated.co/spatial übernommen. Einige Fehler waren aber enthalten, weshalb an einigen Stellen die CSS Eigenschaften angepasst werden mussten. Enjoy!

ENGLISH 

# Sourcecode for benjamindornfeld.de

This repository contains the source code for the website benjamindornfeld.de. The code is largely taken from https://templated.co/spatial. But some errors were included, so the CSS properties had to be adjusted in some places.
